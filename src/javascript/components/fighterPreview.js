import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    attributes: fighter
  });

  // todo: show fighter info (image, name, health, etc.)
  if(!fighter)return;
  const fighterDetailsList = Object.entries(fighter);
  const fighterDetailsContainer = createElement({ tagName: 'div', className: 'fighter___details' });
  const fighterImg = createFighterImage(fighter);
  fighterImg.style.height = '300px';
  if(position === 'right'){
    fighterImg.style.transform = 'scaleX(-1)';
  }
  fighterDetailsList
    .slice(1, fighterDetailsList.length - 1)
    .forEach( detail =>{
      const el = createElement({ tagName: 'div', className: 'fighter___detail' });

      el.innerText = detail.join(' ');
      fighterDetailsContainer.appendChild(el);
  });
  
  fighterElement.append(fighterDetailsContainer, fighterImg);
  return fighterElement;
}



export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

