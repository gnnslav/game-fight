import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const { PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock, PlayerOneCriticalHitCombination, PlayerTwoCriticalHitCombination } = controls;
  const firstPlayer = {
    ...firstFighter,
    attackKey: PlayerOneAttack || PlayerOneCriticalHitCombination,
    defendKey: PlayerOneBlock,
  };
  const secondPlayer = {
    ...secondFighter,
    attackKey: PlayerTwoAttack || PlayerTwoCriticalHitCombination,
    defendKey: PlayerTwoBlock,
  }
  const press = 'keypress' || 'keydown';
  const eCodes = [];
  return new Promise((resolve) => {
    //resolve the promise with the winner when fight is over
    const startFight = (e)=>{
      eCodes.push(e.code);
      if(eCodes.length === 2){
        const attackPlayer = getAttacker(eCodes[0], firstPlayer) || getAttacker(eCodes[0], secondPlayer);
        const defendPlayer = getDefender(eCodes[1], firstPlayer) || getDefender(eCodes[1], secondPlayer);
        if(!attackPlayer || !defendPlayer){
          eCodes.length = 0;
          return;
        };
        const getHealth = getDamage(attackPlayer, defendPlayer);
        defendPlayer.health = defendPlayer.health - getHealth;
        indicatorHealth();
        eCodes.length = 0;
      }
        stopFight();
    }
    const stopFight = ()=>{
      if(firstPlayer.health <= 0 || secondPlayer.health <= 0){
        const getWinner = firstPlayer.health <= 0 ? secondPlayer : firstPlayer;
        resolve(getWinner);
      }
    }
  
    function getAttacker(key, player){  
      const attacker = (key === player.attackKey) ? player : null;
      return attacker;
    }
  
    function getDefender(key, player){
      const defender = (key === player.defendKey) ? player : null;
      return defender;
    }
  
    function indicatorHealth(){
      const indicatorLeft = document.getElementById('left-fighter-indicator');
      const indicatorRight = document.getElementById('right-fighter-indicator');
      const startHealthOne = firstFighter.health;
      const startHealthTwo = secondFighter.health;
  
      indicatorLeft.style.width = (firstPlayer.health * 100 / startHealthOne) + '%';
      indicatorRight.style.width = (secondPlayer.health * 100 / startHealthTwo) + '%';
    }
  
    document.addEventListener(press, (e)=>{startFight(e)});  
  });
}

export function getDamage(attacker, defender) {
  const hitPower = (attacker) ? getHitPower(attacker) : 0;
  const blockPower = (defender) ? getBlockPower(defender) : 0;
  const damage = (hitPower > blockPower) ? (hitPower - blockPower) : 0;

  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = randomNum(1, 2);
  const power = attack * criticalHitChance;
  // return hit power
  return power;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = randomNum(1, 2);
  const power = defense * dodgeChance;
  // return block power
  return power;
}

function randomNum(min, max){
  return Math.random() * (max - min) + 1;
}

